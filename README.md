# Software Test Engineer - API
# CoinGecko Software Test Engineer - OHLC API Assessment

You are tasked to design test cases and creating a Postman collection for the OHLC endpoint. 
For context, [OHLC](https://www.investopedia.com/terms/o/ohlcchart.asp) refers to Open, High, Low, Close prices. OHLC is usually used in candlestick chart.
A Sample Postman Collection is provided. You can use it as a quickstart template if needed.

## 1. Product Specifications

The OHLC API should return a list of OHLC data for the target coin. 

The data granularity should be automatically set based on the days value:

- 1 - 2 days: 30 minutes
- 3 - 30 days: 4 hours
- 31 days and beyond: 4 days

Note: Data granularity refers to the timestamp interval between data points. For example, a granularity of 30 minutes means the timestamp difference between each data points is 30 minutes.

This API will also be used on the website (https://www.coingecko.com/en/coins/bitcoin) and mobile application ([iOS App Store](https://gcko.io/coingecko-ios) or [Google Play Store](https://gcko.io/coingecko-android)). 

You can refer to the API documentations on 
- https://www.coingecko.com/api/documentation
- https://apiguide.coingecko.com/getting-started/introduction

### 1.1. API Request 

Sample Endpoint: https://api.coingecko.com/api/v3/coins/bitcoin/ohlc?vs_currency=usd&days=1&precision=5

#### 1.1.1. Request Parameters

1. coin_id (string): any coin ids from https://api.coingecko.com/api/v3/coins/list?include_platform=false should return a valid response, containing OHLC data of the target coin
2. vs_currency (string): any currencies from https://api.coingecko.com/api/v3/simple/supported_vs_currencies should return a valid response, with the price value matching the appropriate currency
3. days (string): only values that matches one of the following should be accepted (1/7/14/30/90/180/365/max),  response timestamp should be within the days values. e.g. days=1, timestamp of the oldest data should be 24 hours before time of request
4. precision (string): only values that matches one of the following should be accepted (0..18/full). OHLC values should be formatted to match the precision. e.g. precision=1, OHLC should only have 1 decimal place

### 1.2. API Sample Response
```
[
  [
    1689211800000, // Timestamp
    30343.47472,   // Open
    30343.47472,   // High
    30320.00696,   // Low
    30320.00696    // Close
  ],
  ...
]
```

## 2. Technical Design

Attached is a technical design draft prepared by the developer. Use this technical design to help in your whitebox/greybox test design. You are free to make any assumptions from the tech diagrams, but state the assumptions in the submission.

![./ohlc_tech-design.png](./ohlc_tech-design.png)


## 3. Scoring Guide

All submissions will be evaluated based on the following criteria

- Organization of Test Case (organized usage of Postman collections, pre-request script, environment variables, assertions, data files)
- Ease of maintenance (updating collections/assertions due to requirements changes/injections)
- Test Coverage (happy and non-happy paths, edge cases, different pre-conditions, related dependencies)
- Application of different testing strategies (blackbox, whitebox, greybox testing)
- Samples of failures scenarios and relevant details (client environment, network trace, steps to reproduce)

## 4. Submission Guide

- Your submission should include the Postman collection (including environment config and data files, if applicable), and any other relevant test artefacts.
- You should fork this repository into a private repository, and upload the (1) Postman Collections into your fork, and (2) any other test artefacts. Invite the users in the [Reviewers](https://gitlab.com/groups/coingecko-dev-test/-/group_members) group with the Reporter role.
- Your submission should give us an overview of your **depth and breadth** of working experience to understand how to help you smoothly onboard onto the team.
- We do not expect candidates that meet the baseline to take more than 2 weeks to complete the assignment. Do correspond with the Talent Acquisition team for extensions.
- Your submission should give us a overview of your depth and breadth of working experience to understand how to help you smoothly onboard onto the team.
- Your submission will be used as a foundation for the next/final round of interview.
- You may use our [stackshare.io](https://stackshare.io/coingecko) profile as a point of reference.
